﻿namespace Weather
{
    using Weather.Views;
    using Xamarin.Forms;
    public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();
			
		}

		protected override void OnStart ()
		{
            MainPage = new LoginPage();
        }

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
