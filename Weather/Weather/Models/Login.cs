﻿namespace Weather.Models
{
    using SQLite.Net.Attributes;
    public class Login
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }

        public override int GetHashCode()
        {
            return Id;
        }
    }

}
