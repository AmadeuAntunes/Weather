﻿namespace Weather.Models
{
    using Newtonsoft.Json;
    using System.Collections.Generic;



    public class ModelState
    {

        [JsonProperty("model.Email")]
        public IList<string> Email { get; set; }

        [JsonProperty("model.ConfirmPassword")]
        public IList<string> ConfirmPassword { get; set; }
    }

    public class Registo
    {

        [JsonProperty("Message")]
        public string Message { get; set; }

        [JsonProperty("ModelState")]
        public ModelState ModelState { get; set; }
    }


    public class Coord
    {

        [JsonProperty("lon")]
        public double lon { get; set; }

        [JsonProperty("lat")]
        public double lat { get; set; }
    }

    public class Weather
    {

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("main")]
        public string main { get; set; }

        [JsonProperty("description")]
        public string description { get; set; }

        [JsonProperty("icon")]
        public string icon { get; set; }
    }

    public class Main
    {

        [JsonProperty("temp")]
        public double temp { get; set; }

        [JsonProperty("pressure")]
        public int pressure { get; set; }

        [JsonProperty("humidity")]
        public int humidity { get; set; }

        [JsonProperty("temp_min")]
        public int temp_min { get; set; }

        [JsonProperty("temp_max")]
        public int temp_max { get; set; }
    }

    public class Wind
    {

        [JsonProperty("speed")]
        public double speed { get; set; }
    }

    public class Clouds
    {

        [JsonProperty("all")]
        public int all { get; set; }
    }

    public class Sys
    {

        [JsonProperty("type")]
        public int type { get; set; }

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("message")]
        public double message { get; set; }

        [JsonProperty("country")]
        public string country { get; set; }

        [JsonProperty("sunrise")]
        public int sunrise { get; set; }

        [JsonProperty("sunset")]
        public int sunset { get; set; }
    }

    public class Tempo
    {

        [JsonProperty("coord")]
        public Coord Coord { get; set; }

        [JsonProperty("weather")]
        public IList<Weather> Weather { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("main")]
        public Main Main { get; set; }

        [JsonProperty("visibility")]
        public int Visibility { get; set; }

        [JsonProperty("wind")]
        public Wind Wind { get; set; }

        [JsonProperty("clouds")]
        public Clouds Clouds { get; set; }

        [JsonProperty("dt")]
        public int Dt { get; set; }

        [JsonProperty("sys")]
        public Sys Sys { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("cod")]
        public int Cod { get; set; }
    }
}
