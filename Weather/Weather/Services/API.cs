﻿namespace Weather.Services
{
    using Newtonsoft.Json;
    using System;
    using System.Text;
    using System.Threading.Tasks;
    using System.Net.Http;
    using Weather.Models;
    using System.Net;
    using System.IO;
    using System.Net.Http.Headers;

    public class API
    {

        public static async Task<TokenResponse> GetToken(string userName, string password)
        {
            try
            {
                var cliente = new HttpClient();
                cliente.BaseAddress = new Uri("http://imomatch-001-site2.ctempurl.com");
                var response = await cliente.PostAsync("Token",
                    new StringContent(
                        string.Format("grant_type=password&userName={0}&password={1}", userName, password),
                        Encoding.UTF8,
                        "application/x-www-form-urlencoded"
                        )
                 );

                var resultJson = await response.Content.ReadAsStringAsync();

                var result = JsonConvert.DeserializeObject<TokenResponse>(resultJson);

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        internal static async Task<Tempo> GetTempoAsync(string pesquisaText)
        {



            Tempo returnar = null;
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            try
            {
                HttpResponseMessage response = await client.GetAsync("http://api.openweathermap.org/data/2.5/weather?appid=54787d7fb68e4bd567f3fb3795f24272&lang=pt&units=metric&q=" + pesquisaText.Trim());
                if (response.IsSuccessStatusCode)
                {
                    returnar = JsonConvert.DeserializeObject<Tempo>(response.Content.ReadAsStringAsync().Result);
                }
            }
            catch
            {

            }
            return returnar;


        }

        public static async Task<Registo> Registar(string userName, string password)
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://imomatch-001-site2.ctempurl.com/");

            string jsonData = "{\"Email\":\"" + userName + "\"," + "\"Password\":\"" + password + "\"," + "\"ConfirmPassword\":\"" + password + "\"}";

            var content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync("api/Account/Register", content);
            Registo result = null;
           
            try
            {
                result = JsonConvert.DeserializeObject<Registo>(response.Content.ReadAsStringAsync().Result);
                if ("The request is invalid." == result.Message.ToString())
                {
                    result.Message = "O e-mail introduzido já existe";
                }
            }
            catch (Exception)
            {


            }
            return result;
        }
    }
}
