﻿namespace Weather.Services
{
    using Plugin.Connectivity;
    public class Internet
    {
        public static async System.Threading.Tasks.Task<bool> CheckAsync()
        {

            if (!CrossConnectivity.Current.IsConnected)
            {
                return false;
            }

            var isReachable = await CrossConnectivity.Current.IsRemoteReachable("google.com");

            if (!isReachable)
            {
                return false;
            }

           return true;
        }
    }
}
