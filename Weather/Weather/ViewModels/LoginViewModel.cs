﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.Net.Mail;
    using System.Windows.Input;
    using Weather.Models;
    using Weather.Services;
    using Weather.Views;
    using Xamarin.Forms;


    public class LoginViewModel : BaseViewModel
    {

        #region Events

        // public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Atributes
        private string _email;
        private string _password;
        private DataService DataService;
        private bool _isRemebered;
        #endregion

        #region Properties

        public bool IsRemebered
        {
            get { return _isRemebered; }

            set
            {
                SetValue(ref this._isRemebered, value);
            }
        }



        public string Email
        {
            get { return _email; }

            set
            {
                SetValue(ref this._email, value);
            }
        }

        public string Password
        {
            get { return _password; }

            set
            {
                SetValue(ref this._password, value);
            }
        }
        #endregion

        #region Commands


        public ICommand CommandRegistar
        {
            get
            {
                return new RelayCommand(Registar);
            }
        }

        private void Registar()
        {
            MainViewModel.GetInstance().PaginaRegistar = new PaginaRegistarViewModel();
            Application.Current.MainPage.Navigation.PushModalAsync(new RegistarPage());

        }

        public ICommand CommandEntrar
        {
            get
            {
                return new RelayCommand(Login);
            }
        }



        private async void Login()
        {

            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza o E-mail", "OK");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza a Senha", "OK");
                return;
            }

            if (!IsValidEmail(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Introduza um E-mail Valido", "OK");
                return;
            }

            if (!await Internet.CheckAsync())
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Não existe internet", "OK");
                return;
            }
            var tokenResponse = await API.GetToken(this.Email, this.Password);

            if(!string.IsNullOrEmpty(tokenResponse.ErrorDescription))
              {
                  await Application.Current.MainPage.DisplayAlert("Erro", tokenResponse.ErrorDescription, "OK");
                  return;
              }
              


            try
            {

                if(IsRemebered)
                {
                    Login login = new Login();
                    login.Id = 1;
                    login.Email = Email;
                    login.Senha = Password;
                    DataService.InsertOrUpdate(login);
                }
              
              

              
                
                MainViewModel.GetInstance().PaginaPrincipal = new PaginaPrincipalViewModel();
                await Application.Current.MainPage.Navigation.PushModalAsync(new PaginaPrincipal());

            }
            catch (Exception)
            {

                throw;
            }


        }

        bool IsValidEmail(string email)
        {

            try
            {
                var addr = new MailAddress(email);
                if (addr.Address == email)
                {
                     if (new EmailAddressAttribute().IsValid(email))
                      {
                        return true;
                      }
                    return false;
                  
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Constructores

        public LoginViewModel()
        {

            DataService = new DataService();
            this.IsRemebered = true;
            var x = DataService.First<Login>(false);
            if (x != null)
            {
                Email = x.Email;
                Password = x.Senha;
            }
           
        }

        #endregion


    }
}
