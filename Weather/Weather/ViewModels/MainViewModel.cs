﻿namespace Weather.ViewModels
{
    public class MainViewModel
    {
        #region ViewModels

        public LoginViewModel Login { get; set; }

        public PaginaPrincipalViewModel PaginaPrincipal { get; set; }

        public PaginaAutorViewModel PaginaAutor { get; set; }

        public PaginaTempoViewModel PaginaTempo { get; set; }

        public PaginaRegistarViewModel PaginaRegistar { get; set; }



        #endregion

        #region Properties

        #endregion

        #region Constructores

        public MainViewModel()
        {
            instance = this;
            this.Login = new LoginViewModel();

        }

        #endregion

        #region Singleton

        private static MainViewModel instance;


        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }
            else
            {
                return instance;
            }

        }

        #endregion
    }

}