﻿namespace Weather.ViewModels
{
    public class PaginaPrincipalViewModel
    {
        public PaginaPrincipalViewModel()
        {
            MainViewModel.GetInstance().PaginaAutor = new PaginaAutorViewModel();
            MainViewModel.GetInstance().PaginaTempo = new PaginaTempoViewModel();
        }
    }

}