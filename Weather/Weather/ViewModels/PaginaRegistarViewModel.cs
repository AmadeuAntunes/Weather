﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Net.Mail;
    using System.Windows.Input;
    using Weather.Services;
    using Weather.Views;
    using Xamarin.Forms;

    public class PaginaRegistarViewModel : BaseViewModel
    {

        private string _email;
        private string _password;
        private string _PasswordRepete;

        public string PasswordRepete
        {
            get { return _PasswordRepete; }

            set
            {
                SetValue(ref this._PasswordRepete, value);
            }
        }



        public string Email
        {
            get { return _email; }

            set
            {
                SetValue(ref this._email, value);
            }
        }

        public string Password
        {
            get { return _password; }

            set
            {
                SetValue(ref this._password, value);
            }
        }



        public ICommand CommandCancelar
        {
            get
            {
                return new RelayCommand(Cancelar);
            }
        }
        public ICommand CommandRegistar
        {
            get
            {
                return new RelayCommand(Registar);
            }
        }

        private async void Registar()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza o E-mail", "OK");
                return;
            }
            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza a Senha", "OK");
                return;
            }
            if (string.IsNullOrEmpty(this.PasswordRepete))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Por favor introduza a Senha Novamente", "OK");
                return;
            }

            if (this.PasswordRepete != this.Password)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "As Senhas introduzidas não são iguais", "OK");
                return;
            }

            if (!IsValidEmail(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert("Erro", "Introduza um E-mail Valido", "OK");
                return;
            }

             var resposta =   await API.Registar(this.Email, this.Password);
            if (resposta != null)
            {
                await Application.Current.MainPage.DisplayAlert("Erro", resposta.Message, "OK");
                return;
            }
           

        }


        bool IsValidEmail(string email)
        {

            try
            {
                var addr = new MailAddress(email);
                if (addr.Address == email)
                {
                    /* if (new EmailAddressAttribute().IsValid(email))
                     {

                     }*/
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private async void Cancelar()
        {

            await Application.Current.MainPage.Navigation.PushModalAsync(new LoginPage());
        }
    }
}