﻿namespace Weather.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using System.Linq;
    using System.Windows.Input;
    using Weather.Models;
    using Weather.Services;
    using Weather.Views;
    using Xamarin.Forms;
    public class PaginaTempoViewModel : BaseViewModel
    {
        string _Pais;
        public string Pais
        {
            get { return _Pais; }

            set
            {
                SetValue(ref this._Pais, value);
            }
        }



        string _TempMin;
        public string TempMin
        {
            get { return _TempMin; }

            set
            {
                SetValue(ref this._TempMin, value);
            }
        }



        string _TempMax;
        public string TempMax
        {
            get { return _TempMax; }

            set
            {
                SetValue(ref this._TempMax, value);
            }
        }
        string _Description;

        public string Description
        {
            get { return _Description; }

            set
            {
                SetValue(ref this._Description, value);
            }
        }

        string _PesquisaText;
        public string PesquisaText
        {
            get { return _PesquisaText; }

            set
            {
                SetValue(ref this._PesquisaText, value);
            }
        }
        string _imagem;
        public string Imagem
        {
            get { return _imagem; }

            set
            {
                SetValue(ref this._imagem, value);
            }
        }
        public ICommand CommandPesquisar
        {
            get
            {
                return new RelayCommand(Pesquisa);
            }
        }

        public ICommand SairCommand
        {
            get
            {
                return new RelayCommand(Sair);
            }
        }

        private async void Sair()
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(new LoginPage());
        }

        public async void Pesquisa()
        {
            Tempo api = new Tempo();
            api = await API.GetTempoAsync(PesquisaText);
         

          try
          {
              var icon = api.Weather.FirstOrDefault().icon;
              var pais = api.Sys.country.ToString();
              Imagem = "https://openweathermap.org/themes/openweathermap/assets/vendor/owm/img/widgets/" + icon + ".png";
              Description = api.Weather.FirstOrDefault().description;
              TempMax = "Max: " + api.Main.temp_max.ToString() + "ºC";
              TempMin = "Min: " + api.Main.temp_min.ToString() + "ºC";
              Pais = "Pais: " + pais;

          }
          catch (System.Exception)
          {

              Application.Current.MainPage.DisplayAlert("Error", "Error", "Error");
          }








        }

        public PaginaTempoViewModel()
        {

        }
    }
}
