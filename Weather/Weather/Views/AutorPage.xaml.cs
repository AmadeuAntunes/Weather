﻿namespace Weather.Views
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;
    using System;
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AutorPage : ContentPage
	{
		public AutorPage ()
		{
			InitializeComponent ();
		}
        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Device.OpenUri(new Uri("https://amadeuantunes.com"));
        }

    }
}